##Ngin-RTMP-Module 斗鱼修改版

###项目简介
    
本项目是在开源项目nginx-rtmp-module上修改，为适配斗鱼业务需求而开发的程序；

主要添加的功能：

- pull操作前执行get鉴权操作，如果不能匹配期望值，则中断操作；
- 根据tcurl实现多域名支持；
- tcurl跨层级穿越传输；


### Download & run
    docker pull 124.126.126.97:5000/rtmpsvr:0.1.3
    docker stop $(docker ps -aq)
    rm -f /var/ngxrtmp/conf/*
    docker run -d --net=host -v /var/ngxrtmp:/var/ngxrtmp 124.126.126.97:5000/rtmpsvr:0.1.3 /bin/rtmpsvr fringe reset
    
    docker run -d --net=host -v /var/ngxrtmp:/var/ngxrtmp 124.126.126.97:5000/rtmpsvr:0.1.3 /bin/rtmpsvr parent reserve
    
    
    docker run -d --net=host -v /var/ngxrtmp:/var/ngxrtmp 124.126.126.97:5000/rtmpsvr:0.1.3 /bin/rtmpsvr root reset
    docker run -ti --net=host -v /var/ngxrtmp:/var/ngxrtmp 124.126.126.97:5000/rtmpsvr:0.1.3 /bin/bash


    ffmpeg -re -i 2.5MCBR_aac.ts -c copy -bsf:a aac_adtstoasc -f flv rtmp://58.221.5.13/master/test


### Docker Setting

    echo "INSECURE_REGISTRY='--insecure-registry 124.126.126.97:5000'" >> /etc/sysconfig/docker （Centos7配置）
    systemctl restart docker
    echo "DOCKER_OPTS='--insecure-registry 182.140.222.138:5000'" >> /etc/default/docker （Ubuntu14.04配置）
    service docker restart


### 防火墙设置

    iptables -I INPUT 1 -p tcp -m tcp --dport 1935 -j ACCEPT
    iptables-save
    
### 模拟鉴权服务器

    gunicorn -k gevent -w 4 -b 0.0.0.0:80 flask_test:app


### 联系我

    email：hiccupzhu@189.cn


