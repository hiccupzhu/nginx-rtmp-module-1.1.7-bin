FROM 124.126.126.97:5000/ubuntu:14.04.2

MAINTAINER Zhu Shiqi, hiccupzhu@163.com

RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ADD sources.list /etc/apt/
# install required packages
RUN apt-get update

ADD nginx.*.gz /bin/
ADD rtmpsvr /bin/
ADD *.conf /etc/


RUN gunzip -f /bin/nginx.*.gz 
RUN chmod +x /bin/nginx.*
RUN ln -sf /bin/nginx.1.7.11 /bin/nginx
RUN apt-get install -y vim wget


CMD /bin/rtmpsvr fringe reserve
