#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Oct 14, 2015

@author: szhu
'''

from flask import Flask, render_template
from flask.globals import request
import logging.handlers

app = Flask('flask_test');



logger = logging.getLogger('flask_test');


formatter = logging.Formatter('[%(asctime)s] %(levelname)s %(message)s');
logger.addHandler(logging.StreamHandler());
    
file_handler = logging.handlers.RotatingFileHandler(
    './flask_test.log',
    maxBytes = 0x800000,
    backupCount = 8);
file_handler.setFormatter(formatter);
file_handler.setLevel(logging.DEBUG);
logger.addHandler(file_handler);


logger.setLevel(logging.DEBUG);


@app.route('/')
def _index_():
     return render_template('index.html');


@app.route('/api/live/check', methods=['GET', 'POST'])
def api_live_check():
    logger.info( "path:%s  form:%s  args:%s  remote_addr:%s"  % (request.path, str(request.form), str(request.args), str(request.remote_addr)));
    
#     if(request.args['token'] == '296662695') :
#         return '1';
#     else:
#         return '0';
    return '1';


###########################################################################################3
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True);